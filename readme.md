#### huawei_music
+ 该项目为完成华为云python学习路线第一阶段的最后实战
1. 任务 1，使用 Python 制作一个简易的音乐播放器。
   
    使用 GUI 工具搭建图形化界面（PyQt5/tkinter 等）。 
   
    音乐列表内容为本地指定文件夹下的音乐文件。 
   
    具备点击播放、下一首、上一首等功能。

    注意事项：
   
    Tkinter/PyQt5：Python GUI 编程工具。 
   
    可与使用多线程/多进程。 
   
    使用函数或者类的形式完成。

2. 记录播放音乐及时间（2021-04-21 16:55:24 xxx.mp3），并使用 Pandas、matplotlib 等工具对记
录分析及可视化。
   1. 记录写入本地文件 log.txt 中，每一行为一条记录。 
   2. 在添加功能时不修改原有代码。 
   3. 分析部分的代码可以在 jupyter 中进行。
   
3.  任务 3，获取音乐文件，并对播放器进行测试。
    
   获取音乐文件放入播放器指定路径下。
   测试是否能正常执行。 （此处获取音乐可以通过 requests 模块获取一些音乐网站上的音乐。）