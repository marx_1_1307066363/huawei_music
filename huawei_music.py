import tkinter as tk
import os
import pygame as pg
import time


# 写一个装饰器，生成日志
def music_log(func):
    def inner(*args, **kwargs):
        t = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        # print(type(t))
        if func:
            # print(func.__name__)
            t += '\t{}\n'.format(func(*args, **kwargs))
            # print(t)
            with open('log.txt', 'a', encoding='utf8') as f:
                f.write(t)

    return inner


class MusicGo:
    def __init__(self, path=None):
        if path:
            self.path = path
        else:
            self.path = os.curdir
        self.song = None
        self.count = 0
        self.root = tk.Tk()
        self.cur_song = tk.StringVar(value='None')
        # self.call_song = None
        self.root.geometry('500x150')
        self.root.title('MusicGo')
        self.detail()

    def detail(self):
        root = self.root

        # 歌曲
        self.open_dir()  # 打开歌单
        # self.cur_song = tk.StringVar(value=self.song[self.count])
        song = tk.Label(root, textvariable=self.cur_song, bg='pink',
                        font=('黑体', 12))
        song.place(x=50, y=50, width=400, height=30)

        # 上一首歌
        prev_btn = tk.Button(root, text='上一首歌', bg='pink',
                             font=('黑体', 12), command=self._prev)
        prev_btn.place(x=50, y=100, width=75, height=30)

        # 下一首歌
        next_btn = tk.Button(root, text='下一首歌', bg='pink',
                             font=('黑体', 12), command=self._next)
        next_btn.place(x=375, y=100, width=75, height=30)

        # 播放
        go_btn = tk.Button(root, text='播放', bg='pink',
                           font=('黑体', 12), command=self._go)
        go_btn.place(x=200, y=100, width=50, height=30)

        # 暂停
        pause_btn = tk.Button(root, text='暂停', bg='pink',
                              font=('黑体', 12), command=self._pause)
        pause_btn.place(x=250, y=100, width=50, height=30)

        root.mainloop()

    def open_dir(self):
        # path = self.path
        # os.system("cd {}".format(path))
        # print(os.listdir(path))
        self.song = os.listdir(self.path)
        self.cur_song = tk.StringVar(value=self.song[self.count])

    @music_log
    def _go(self):
        song_name = self.cur_song.get()
        song = os.path.join('', self.path, song_name)
        # print(song)
        # print(self.cur_song.get())
        pg.mixer.init()
        pg.mixer.music.load(song)
        pg.mixer.music.play()
        # return song_name + '\t播放'
        return song_name

    # @music_log
    def _pause(self):
        if self.cur_song:
            # pg.mixer.init()
            pg.mixer.music.pause()
        # return self.cur_song.get() + '\t暂停'
        return self.cur_song.get()

    def _prev(self):
        # length = len(self.song)
        self.count = self.count - 1 if self.count > 0 else len(self.song) - 1
        self.cur_song.set(self.song[self.count])
        self._go()

    def _next(self):
        self.count = self.count + 1 if self.count < len(self.song) - 1 else 0
        self.cur_song.set(self.song[self.count])
        self._go()


if __name__ == '__main__':
    path = 'E:\\CloudMusic'
    # music_log()()
    MusicGo(path)
