import matplotlib.pyplot as plt
import pandas as pd
from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['SimHei']  # 解决中文不显示问题


# 打印pandas.DateFrame
class PrintLog:
    def __init__(self, path):
        self.path = path
        self.log = self.load_log()

    def load_log(self):
        return pd.read_csv(path, sep='\t', names=['Date and Time', 'Name'])


if __name__ == '__main__':
    path = 'log.txt'
    log = PrintLog(path).log
    print(log)
    plt.subplot(111)
    plt.hist(log)
    # plt.legend()
    plt.show()
