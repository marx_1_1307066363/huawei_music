import requests as req
import huawei_music as hm

Header = {
    'User-Agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64)' +
        ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 HBPC/11.0.7.301'
}
Url = 'https://music.163.com/#/search/m/?s={}'


# 通过歌手获取歌曲列表
def get_song(artist_name):
    res = req.request('get', url=Url.format(artist_name), headers=Header)
    print(res.text)


if __name__ == '__main__':
    artist = '邓紫棋'
    get_song(artist)
